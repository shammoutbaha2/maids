# Task Manager App

## Project Overview

This is a Task Manager App built with Flutter as part of a test project. The app allows users to manage their tasks efficiently, featuring user authentication, task CRUD operations, pagination, state management, local storage, and comprehensive unit tests.

## Key Features

1. **User Authentication**:

   - Implemented secure user authentication using [DummyJSON Auth API](https://dummyjson.com/docs/auth).
   - Users can log in with their username and password.

2. **Task Management**:

   - Users can view, add, edit, and delete tasks.
   - Task data is managed using the [DummyJSON Todos API](https://dummyjson.com/docs/todos).

3. **Pagination**:

   - Efficiently fetch a large number of tasks with pagination.
   - Utilized the `https://dummyjson.com/todos?limit=10&skip=10` endpoint for pagination.

4. **State Management**:

   - State management implemented using [Bloc/Cubit](https://pub.dev/packages/flutter_bloc).
   - Ensures efficient state updates across the app.

5. **Local Storage**:

   - Tasks are persisted locally using Flutter's [shared_preferences](https://pub.dev/packages/shared_preferences) and [sqflite](https://pub.dev/packages/sqflite).
   - Ensures tasks are accessible even when the app doesn't have connection to internet or when the server is down.

6. **Unit Tests**:
   - Comprehensive unit tests cover critical functionalities such as task CRUD operations, input validation, and network requests.

## Installation

1. Clone the repository:
   - git clone https://gitlab.com/shammoutbaha2/maids
   - cd maids
   - run in terminal flutter pub get
   - run flutter run
