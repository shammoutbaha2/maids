import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maids/src/core/routes/app_routes.dart';
import 'package:maids/src/core/service/sl.dart';
import 'package:maids/src/core/theme/theme.dart';
import 'package:maids/src/modules/splash/presentations/pages/splash_screen.dart';
import 'package:oktoast/oktoast.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initSL();
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return OKToast(
      child: GetMaterialApp(
        theme: AppTheme.base(),
        debugShowCheckedModeBanner: false,
        getPages: AppPages.pages,
        initialRoute: Pages.splash.route,
        home: const SplashScreen(),
      ),
    );
  }
}
