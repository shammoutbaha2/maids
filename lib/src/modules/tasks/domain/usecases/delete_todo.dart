import 'package:dartz/dartz.dart';
import 'package:maids/src/core/errors/failures.dart';
import 'package:maids/src/core/usecases/app_usecases.dart';
import 'package:maids/src/modules/tasks/domain/repo/todo_repo.dart';

class DeleteTodoUC extends UseCaseTypeAndParams<Unit, int> {
  final TodoRepo repo;

  DeleteTodoUC({required this.repo});

  @override
  Future<Either<Failure, Unit>> call(int params) => repo.deleteTodo(id: params);
}
