import 'package:dartz/dartz.dart';
import 'package:maids/src/core/errors/failures.dart';
import 'package:maids/src/core/usecases/app_usecases.dart';
import 'package:maids/src/modules/tasks/domain/entities/manage_todo.dart';
import 'package:maids/src/modules/tasks/domain/repo/todo_repo.dart';

class AddTodoUC extends UseCaseTypeAndParams<Unit, ManageTodo> {
  final TodoRepo repo;

  AddTodoUC({required this.repo});

  @override
  Future<Either<Failure, Unit>> call(ManageTodo params) =>
      repo.addTodo(todo: params);
}
