import 'package:dartz/dartz.dart';
import 'package:maids/src/core/errors/failures.dart';
import 'package:maids/src/core/usecases/app_usecases.dart';
import 'package:maids/src/modules/tasks/domain/entities/task_entity.dart';
import 'package:maids/src/modules/tasks/domain/repo/todo_repo.dart';

class GetMyTasks extends UseCaseType<List<Todo>> {
  final TodoRepo repo;

  GetMyTasks({required this.repo});

  @override
  Future<Either<Failure, List<Todo>>> call() => repo.getMyTasks();
}
