import 'package:dartz/dartz.dart';
import 'package:maids/src/core/errors/failures.dart';
import 'package:maids/src/core/usecases/app_usecases.dart';
import 'package:maids/src/modules/tasks/domain/entities/task_entity.dart';
import 'package:maids/src/modules/tasks/domain/repo/todo_repo.dart';

class GetTodoUC extends UseCaseTypeAndParams<List<Todo>, int> {
  final TodoRepo repo;

  GetTodoUC({required this.repo});

  @override
  Future<Either<Failure, List<Todo>>> call(int params) =>
      repo.getTodo(page: params);
}
