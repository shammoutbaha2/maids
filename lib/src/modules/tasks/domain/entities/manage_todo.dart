class ManageTodo {
  String? todo;
  bool completed;
  int? userId;

  int? todoId;
  ManageTodo({this.todo, this.completed = false, this.userId, this.todoId});

  
}
