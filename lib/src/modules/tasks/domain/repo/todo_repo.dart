import 'package:dartz/dartz.dart';
import 'package:maids/src/modules/tasks/domain/entities/manage_todo.dart';
import 'package:maids/src/modules/tasks/domain/entities/task_entity.dart';

import '../../../../core/errors/failures.dart';

abstract class TodoRepo {
  Future<Either<Failure, List<Todo>>> getTodo({required int page});

  Future<Either<Failure, List<Todo>>> getMyTasks();
  Future<Either<Failure, Unit>> addTodo({required ManageTodo todo});
  Future<Either<Failure, Unit>> editTodo({required ManageTodo todo});
  Future<Either<Failure, Unit>> deleteTodo({required int id});
}
