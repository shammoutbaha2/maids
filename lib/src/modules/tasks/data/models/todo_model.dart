import 'package:maids/src/modules/tasks/domain/entities/task_entity.dart';

class TodoModel extends Todo {
  TodoModel(
      {required super.id,
      required super.todo,
      required super.completed,
      required super.userId});

  Todo toEntity() {
    return Todo(
      id: id,
      todo: todo,
      completed: completed,
      userId: userId,
    );
  }

  factory TodoModel.fromJson(Map<String, dynamic> json) {
    return TodoModel(
        id: json["id"],
        todo: json["todo"],
        completed: json["completed"] == true || json["completed"] == 1,
        userId: json["userId"]);
  }

  static List<TodoModel> listFromJson(List list) {
    return list.map((e) => TodoModel.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "todo": todo,
        "completed": completed ? 1 : 0,
        "userId": userId,
      };
}
