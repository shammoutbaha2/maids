import 'package:maids/src/core/helper/storage_helper.dart';
import 'package:maids/src/modules/tasks/domain/entities/manage_todo.dart';

import '../../../../core/service/sl.dart';

class ManageTodoModel extends ManageTodo {
  ManageTodoModel(
      {required super.todo,
      required super.completed,
      required super.todoId,
      required super.userId});

  factory ManageTodoModel.fromEntity(ManageTodo manageTodo) => ManageTodoModel(
        todo: manageTodo.todo,
        todoId: manageTodo.todoId,
        userId: manageTodo.userId,
        completed: manageTodo.completed,
      );
  Map<String, dynamic> toJson() => {
        if (todoId != null) "id": todoId,
        "todo": todo,
        "completed": completed,
        "userId": sl<StorageHelper>().getInt('userId'),
      };
}
