import 'package:dartz/dartz.dart';
import 'package:maids/src/core/errors/failures.dart';
import 'package:maids/src/core/functions/print.dart';
import 'package:maids/src/core/helper/storage_helper.dart';
import 'package:maids/src/modules/tasks/data/data_source/todo_local_ds.dart';
import 'package:maids/src/modules/tasks/data/data_source/todo_remote_ds.dart';
import 'package:maids/src/modules/tasks/data/models/manage_todo.dart';
import 'package:maids/src/modules/tasks/data/models/todo_model.dart';
import 'package:maids/src/modules/tasks/domain/entities/manage_todo.dart';
import 'package:maids/src/modules/tasks/domain/entities/task_entity.dart';
import 'package:maids/src/modules/tasks/domain/repo/todo_repo.dart';

import '../../../../core/constant/const_values.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/functions/check_connection.dart';
import '../../../../core/service/sl.dart';
import '../../presentation/manager/todo_controller.dart';

class TodoRepoImpl extends TodoRepo {
  final TodoRemoteDS remoteDS;
  final TodoLocalDataSource localDataSource;

  TodoRepoImpl({required this.remoteDS, required this.localDataSource});

  @override
  Future<Either<Failure, Unit>> addTodo({required ManageTodo todo}) async {
    if (await ConnectivityHelper.noConnection()) {
      return left(NetworkFailure(ConstantString.noInternetConnection));
    }
    try {
      final result =
          await remoteDS.addTodo(model: ManageTodoModel.fromEntity(todo));
      return result;
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    } catch (e) {
      return Left(UnknownFailure(e.toString()));
    }
  }

  @override
  Future<Either<Failure, Unit>> deleteTodo({required int id}) async {
    if (await ConnectivityHelper.noConnection()) {
      return left(NetworkFailure(ConstantString.noInternetConnection));
    }
    try {
      try {
        await remoteDS.deleteTodo(id: id);
      } catch (_) {}
      localDataSource.delete(id);
      return const Right(unit);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    } catch (e) {
      return Left(UnknownFailure(e.toString()));
    }
  }

  @override
  Future<Either<Failure, Unit>> editTodo({required ManageTodo todo}) async {
    if (await ConnectivityHelper.noConnection()) {
      return left(NetworkFailure(ConstantString.noInternetConnection));
    }
    try {
      try {
        await remoteDS.editTodo(model: ManageTodoModel.fromEntity(todo));
      } catch (_) {}
      final model = TodoModel.fromJson({
        "id": todo.todoId,
        "todo": todo.todo,
        "userId": sl<StorageHelper>().getInt('userId'),
        "completed": todo.completed ? 1 : 0
      });
      await localDataSource.update(model);
      return const Right(unit);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    } catch (e) {
      return Left(UnknownFailure(e.toString()));
    }
  }

  @override
  Future<Either<Failure, List<Todo>>> getTodo({required int page}) async {
    if (await ConnectivityHelper.noConnection()) {
      return getSavedOrder(NetworkFailure(ConstantString.noInternetConnection));
    }
    try {
      final result = await remoteDS.getTodo(page: page);
      return result;
    } on ServerException catch (e) {
      return getSavedOrder(ServerFailure(e.message));
    } catch (e) {
      return getSavedOrder(UnknownFailure(e.toString()));
    }
  }

  @override
  Future<Either<Failure, List<Todo>>> getMyTasks() async {
    if (await ConnectivityHelper.noConnection()) {
      return getSavedOrder(NetworkFailure(ConstantString.noInternetConnection),
          byUser: true);
    }
    try {
      final result = await remoteDS.getMyTasks();
      final localResult =
          await getSavedOrder(UnknownFailure('Unknown error'), byUser: true);
      if (result.isRight() || localResult.isRight()) {
        final List<TodoModel> myTodo = result.getOrElse(() => []);
        final List<TodoModel> myTodo2 = localResult.getOrElse(() => []);
        return Right([...myTodo2, ...myTodo]);
      }
      return result;
    } on ServerException catch (e) {
      return getSavedOrder(ServerFailure(e.message), byUser: true);
    } catch (e) {
      return getSavedOrder(UnknownFailure(e.toString()), byUser: true);
    }
  }

  Future<Either<Failure, List<TodoModel>>> getSavedOrder(Failure fail,
      {bool byUser = false}) async {
    try {
      if (!byUser && sl<TodoController>().getData.isNotEmpty) {
        return const Right([]);
      }
      List data = await localDataSource.getData(
          query:
              byUser ? {"userId": sl<StorageHelper>().getInt('userId')} : null);
      pr("HERE IS SAVED DATA  $data");
      return Right(TodoModel.listFromJson(data));
    } on CacheException catch (e) {
      return Left(CacheFailure(e.message));
    } catch (e) {
      return Left(fail);
    }
  }
}
