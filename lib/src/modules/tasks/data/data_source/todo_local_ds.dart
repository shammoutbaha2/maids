import 'package:maids/src/core/constant/const_values.dart';
import 'package:maids/src/modules/tasks/data/models/todo_model.dart';
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart' as sql;

import '../../../../core/errors/exceptions.dart';
import '../../../../core/errors/failures.dart';
import '../../../../core/helper/local_storage_helper.dart';

class TodoLocalDataSource extends DBHelper<TodoModel> {
  final String name = "Task";

  static sql.Database? _database;

  @override
  Future initDatabase() async {
    if (_database != null) return;
    await database();
  }

  @override
  Future database() async {
    String dbPath = await sql.getDatabasesPath();
    _database = await sql.openDatabase(path.join(dbPath, 'tasks.db'),
        onCreate: (sql.Database db, int version) {
      return db.execute(
          'CREATE TABLE Task (id INTEGER  PRIMARY KEY , todo TEXT , userId INTEGER , completed INTEGER )');
    }, version: 1);
  }

  @override
  Future<List> getData({Map<String, dynamic>? query}) async {
    try {
      if (query != null) {
        return await _database!.rawQuery(
            "SELECT * FROM Task WHERE ${query.keys.first} =?",
            [query.values.first]);
      }
      return await _database!.query(name);
    } catch (e) {
      throw CacheException(e.toString());
    }
  }

  @override
  Future insert(TodoModel data) async {
    try {
      await _database!.insert(name, data.toJson(),
          conflictAlgorithm: sql.ConflictAlgorithm.replace);
    } catch (e) {
      throw CacheFailure(ConstantString.anErrorOccurred);
    }
  }

  @override
  Future insertList(List<TodoModel> data) async {
    for (var element in data) {
      insert(element);
    }
  }

  @override
  Future<int> delete(int id) async {
    return await _database!.delete(name, where: 'id = ?', whereArgs: [id]);
  }

  @override
  Future<int> update(TodoModel data) async {
    await _database!.rawQuery(
        'UPDATE Task SET todo = ?, completed = ? WHERE id = ?',
        [data.todo, data.completed ? 1 : 0, data.id]);

    return 1;
  }

  Future dropTable() async {
    try {
      await _database!.delete("Task");
    } catch (e) {
      // print(e);
    }
  }
}
