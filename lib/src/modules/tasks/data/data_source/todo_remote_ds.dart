import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:maids/src/core/errors/failures.dart';
import 'package:maids/src/core/helper/dio_helper.dart';
import 'package:maids/src/core/helper/storage_helper.dart';
import 'package:maids/src/modules/tasks/data/data_source/todo_local_ds.dart';
import 'package:maids/src/modules/tasks/data/models/manage_todo.dart';
import 'package:maids/src/modules/tasks/data/models/todo_model.dart';

import '../../../../core/constant/const_values.dart';
import '../../../../core/errors/error_hdl.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/service/sl.dart';

abstract class TodoRemoteDS {
  Future<Either<Failure, List<TodoModel>>> getTodo({required int page});
  Future<Either<Failure, List<TodoModel>>> getMyTasks();
  Future<Either<Failure, Unit>> addTodo({required ManageTodoModel model});
  Future<Either<Failure, Unit>> editTodo({required ManageTodoModel model});
  Future<Either<Failure, Unit>> deleteTodo({required int id});
}

class TodoRemoteDSImpl extends TodoRemoteDS {
  final DioHelper client;

  TodoRemoteDSImpl({required this.client});

  @override
  Future<Either<Failure, List<TodoModel>>> getTodo({required int page}) async {
    try {
      final path = 'todos?limit=10&skip=${10 * (page - 1)}';
      final response = await client.get(
        path: path,
      );
      if (response.statusCode == 200) {
        final responseModel = TodoModel.listFromJson(response.data["todos"]);
        sl<TodoLocalDataSource>().insertList(responseModel);

        return Right(responseModel);
      } else {
        throw ConstantString.anErrorOccurred;
      }
    } on DioException catch (e) {
      throw handleError(e);
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  @override
  Future<Either<Failure, List<TodoModel>>> getMyTasks() async {
    try {
      final userId = sl<StorageHelper>().getInt("userId");
      final path = 'todos/user/$userId';
      final response = await client.get(
        path: path,
      );
      if (response.statusCode == 200) {
        final responseModel = TodoModel.listFromJson(response.data["todos"]);
        return Right(responseModel);
      } else {
        throw ConstantString.anErrorOccurred;
      }
    } on DioException catch (e) {
      throw handleError(e);
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  @override
  Future<Either<Failure, Unit>> addTodo(
      {required ManageTodoModel model}) async {
    try {
      const path = 'todos/add';
      final response = await client.post(
        path: path,
        body: model.toJson(),
      );

      if (response.statusCode == 200) {
        sl<TodoLocalDataSource>().insert(TodoModel.fromJson(
            model.toJson()..addAll({"id": DateTime.now().millisecond})));
        return const Right(unit);
      } else {
        throw ConstantString.anErrorOccurred;
      }
    } on DioException catch (e) {
      throw handleError(e);
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  @override
  Future<Either<Failure, Unit>> deleteTodo({required int id}) async {
    try {
      final path = 'todos/$id';
      final response = await client.delete(
        path: path,
      );
      if (response.statusCode == 200) {
        sl<TodoLocalDataSource>().delete(id);
        return const Right(unit);
      } else {
        throw ConstantString.anErrorOccurred;
      }
    } on DioException catch (e) {
      throw handleError(e);
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  @override
  Future<Either<Failure, Unit>> editTodo(
      {required ManageTodoModel model}) async {
    try {
      final path = 'todos/${model.todoId}';
      final response = await client.update(
        path: path,
        body: model.toJson(),
      );
      if (response.statusCode == 200) {
        sl<TodoLocalDataSource>().update(TodoModel.fromJson(model.toJson()));
        return const Right(unit);
      } else {
        throw ConstantString.anErrorOccurred;
      }
    } on DioException catch (e) {
      throw handleError(e);
    } catch (e) {
      throw ServerException(e.toString());
    }
  }
}
