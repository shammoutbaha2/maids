import 'package:dartz/dartz.dart';
import 'package:maids/src/core/errors/failures.dart';
import 'package:maids/src/core/pagination-bloc/manager/base_pagination.dart';
import 'package:maids/src/modules/tasks/domain/entities/task_entity.dart';
import 'package:maids/src/modules/tasks/domain/usecases/get_todo.dart';

class TodoController extends PaginationCubit<Todo> {
  final GetTodoUC getTodoUC;

  TodoController({required this.getTodoUC}) : super(Initial());

  @override
  Future<Either<Failure, List<Todo>>> apiCall() => getTodoUC(pageNumber);
}
