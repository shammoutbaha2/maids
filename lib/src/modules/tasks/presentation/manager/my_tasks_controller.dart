import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maids/src/core/errors/failures.dart';
import 'package:maids/src/core/pagination-bloc/manager/base_pagination.dart';
import 'package:maids/src/core/widgets/loader_widget.dart';
import 'package:maids/src/core/widgets/scaffold_messenger.dart';
import 'package:maids/src/modules/tasks/domain/entities/task_entity.dart';
import 'package:maids/src/modules/tasks/domain/usecases/add_todo.dart';
import 'package:maids/src/modules/tasks/domain/usecases/delete_todo.dart';
import 'package:maids/src/modules/tasks/domain/usecases/edit_todo.dart';

import '../../domain/entities/manage_todo.dart';
import '../../domain/usecases/get_my_tasks.dart';

class MyTasksController extends PaginationCubit<Todo> {
  final AddTodoUC addTodoUC;
  final DeleteTodoUC deleteTodoUC;
  final EditTodoUC editTodoUC;
  final GetMyTasks getMyTasks;
  MyTasksController(
      {required this.addTodoUC,
      required this.deleteTodoUC,
      required this.getMyTasks,
      required this.editTodoUC})
      : super(Initial());

  @override
  Future<Either<Failure, List<Todo>>> apiCall() => getMyTasks();

  @override
  bool get isOnePage => true;

  final formKey = GlobalKey<FormState>();
  ManageTodo todo = ManageTodo();

  addTask(bool isCompleted) async {
    if (!formKey.currentState!.validate()) return;
    formKey.currentState!.save();

    Get.showOverlay(
        asyncFunction: () async {
          final either = await addTodoUC(todo..completed = isCompleted);
          either.fold((l) => Toasts.toastErrorMessage(l.message), (r) {
            Get.close(1);
            refetchData();
          });
        },
        loadingWidget: const LoadingWidget());
  }

  updateTask(int id, bool isCompleted) async {
    if (!formKey.currentState!.validate()) return;
    formKey.currentState!.save();

    Get.showOverlay(
        asyncFunction: () async {
          final either = await editTodoUC(todo
            ..completed = isCompleted
            ..todoId = id);
          either.fold((l) => Toasts.toastErrorMessage(l.message), (r) {
            Get.close(1);
            refetchData();
          });
        },
        loadingWidget: const LoadingWidget());
  }

  deleteTask(int id) async {
    Get.showOverlay(
        asyncFunction: () async {
          final either = await deleteTodoUC(id);
          either.fold((l) => Toasts.toastErrorMessage(l.message), (r) {
            Get.close(1);
            refetchData();
          });
        },
        loadingWidget: const LoadingWidget());
  }
}
