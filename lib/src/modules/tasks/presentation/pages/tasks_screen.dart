import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maids/src/core/extension/doubles.dart';
import 'package:maids/src/core/pagination-bloc/widgets/paginated_list_view.dart';
import 'package:maids/src/core/routes/app_routes.dart';
import 'package:maids/src/core/service/sl.dart';
import 'package:maids/src/core/widgets/custom_text.dart';
import 'package:maids/src/modules/tasks/presentation/manager/todo_controller.dart';

import '../widgets/task_tile.dart';

class TasksScreen extends StatelessWidget {
  const TasksScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: CustomText(
          text: "Tasks",
          size: 25,
          color: Colors.black,
        ),
        actions: [
          InkWell(
            onTap: () => Get.toNamed(Pages.myTasks.route),
            child: CustomText(
                text: "My Tasks",
                size: 16,
                decoration: TextDecoration.underline,
                color: Colors.black),
          ),
          3.sizedW,
        ],
      ),
      body: PaginationListViewNew(
        separatedWidget: const SizedBox(height: 10),
        controller: sl<TodoController>(),
        itemWidget: (context, index) {
          final task = sl<TodoController>().data[index];
          return TaskTile(task: task);
        },
      ),
    );
  }
}
