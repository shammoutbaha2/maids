import 'package:flutter/material.dart';
import 'package:flutter_dismissible_tile/flutter_dismissible_tile.dart';
import 'package:get/get.dart';
import 'package:maids/src/core/extension/doubles.dart';
import 'package:maids/src/core/pagination-bloc/widgets/paginated_list_view.dart';
import 'package:maids/src/modules/tasks/presentation/manager/my_tasks_controller.dart';
import 'package:maids/src/modules/tasks/presentation/widgets/task_tile.dart';

import '../../../../core/service/sl.dart';
import '../../../../core/widgets/custom_text.dart';
import '../widgets/add_edit_task_bottom_sheet.dart';
import '../widgets/manage_dialog.dart';

class MyTasksScreen extends StatelessWidget {
  const MyTasksScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          onPressed: () => Get.bottomSheet(const ManageTaskBottomSheet()),
          child: const Icon(Icons.add, color: Colors.black)),
      appBar: AppBar(
        centerTitle: true,
        title: CustomText(
          text: "My Tasks",
          size: 25,
          color: Colors.black,
        ),
      ),
      body: PaginationListViewNew(
          separatedWidget: 1.sizedH,
          controller: sl<MyTasksController>(),
          itemWidget: (context, index) => DismissibleTile(
              borderRadius: const BorderRadius.all(Radius.circular(16)),
              delayBeforeResize: const Duration(milliseconds: 500),
              key: UniqueKey(),
              ltrDismissedColor: Colors.lightBlueAccent,
              rtlDismissedColor: Colors.redAccent,
              ltrOverlay: const Text('Edit'),
              ltrOverlayDismissed: const Text('Edited'),
              rtlOverlay: const Text('Delete'),
              rtlOverlayDismissed: const Text('Deleted'),
              confirmDismiss: (direction) async {
                if (direction == DismissibleTileDirection.leftToRight) {
                  Get.bottomSheet(ManageTaskBottomSheet(
                      model: sl<MyTasksController>().data[index]));
                } else {
                  Get.dialog(
                      DeleteDialog(id: sl<MyTasksController>().data[index].id));
                }
                return false;
              },
              onDismissConfirmed: () {},
              child: TaskTile(task: sl<MyTasksController>().data[index]))),
    );
  }
}
