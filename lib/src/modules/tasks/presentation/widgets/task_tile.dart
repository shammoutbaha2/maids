import 'package:flutter/material.dart';
import 'package:maids/src/core/extension/strings.dart';

import '../../../../core/theme/theme.dart';
import '../../../../core/widgets/custom_text.dart';
import '../../domain/entities/task_entity.dart';

class TaskTile extends StatelessWidget {
  const TaskTile({
    super.key,
    required this.task,
  });

  final Todo task;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 3,
      child: ListTile(
        leading: Image.asset(
          "logo.png".imagePath,
          width: 40,
          height: 40,
        ),
        title: CustomText(
          text: task.todo,
          align: TextAlign.start,
          size: 18,
          maxLines: 4,
        ),
        subtitle: CustomText(
          text: task.userId.toString(),
          size: 18,
          maxLines: 4,
        ),
        trailing: CircleAvatar(
          backgroundColor: task.completed
              ? AppTheme.getColorScheme.tertiary
              : AppTheme.getColorScheme.errorContainer,
          child: CustomText(
            text: task.id.toString(),
            size: 18,
            maxLines: 4,
          ),
        ),
      ),
    );
  }
}
