import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:maids/src/core/extension/doubles.dart';
import 'package:maids/src/core/service/sl.dart';
import 'package:maids/src/core/validators/app_validator.dart';
import 'package:maids/src/core/widgets/elevated_button.dart';
import 'package:maids/src/modules/auth/presentation/widgets/text_field.dart';
import 'package:maids/src/modules/tasks/domain/entities/task_entity.dart';
import 'package:maids/src/modules/tasks/presentation/manager/my_tasks_controller.dart';

import '../../../../core/widgets/custom_text.dart';

class ManageTaskBottomSheet extends StatelessWidget {
  const ManageTaskBottomSheet({super.key, this.model});
  final Todo? model;

  @override
  Widget build(BuildContext context) {
    final controller = sl<MyTasksController>();
    RxBool isCompleted = (model?.completed ?? false).obs;

    return BlocBuilder(
      bloc: controller,
      builder: (context, state) => Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 6),
        height: 45.h,
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20))),
        child: SizedBox(
          height: 45.h,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Center(
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(30),
                      child: SizedBox(
                          width: 40.w, child: const Divider(thickness: 4))),
                ),
                5.sizedH,
                Form(
                  key: controller.formKey,
                  child: AuthTextField(
                      initialValue: model?.todo,
                      maxLines: 3,
                      onSaved: (val) {
                        controller.todo.todo = val;
                      },
                      icon: Icons.description,
                      validator: AppValidator.validatorName,
                      text: "ToDo"),
                ),
                4.sizedH,
                Row(
                  children: [
                    Obx(() => Checkbox(
                        value: isCompleted.value,
                        onChanged: (_) => isCompleted(!isCompleted.value))),
                    2.sizedW,
                    CustomText(text: "Completed", size: 16, color: Colors.black)
                  ],
                ),
                4.sizedH,
                AppButton(
                    title: "Save",
                    action: () => model != null
                        ? controller.updateTask(model!.id, isCompleted.value)
                        : controller.addTask(isCompleted.value))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
