import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maids/src/core/extension/doubles.dart';
import 'package:maids/src/modules/tasks/presentation/manager/my_tasks_controller.dart';

import '../../../../core/service/sl.dart';
import '../../../../core/theme/theme.dart';
import '../../../../core/widgets/custom_text.dart';
import '../../../../core/widgets/elevated_button.dart';

class DeleteDialog extends StatelessWidget {
  const DeleteDialog({super.key, required this.id});
  final int id;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: CustomText(text: "Are you sure you want to delete this todo!"),
      actions: [
        SizedBox(
            width: 30.w,
            child: AppButton(
              title: "Ok",
              action: () => sl<MyTasksController>().deleteTask(id),
              color: AppTheme.getColorScheme.error,
            )),
        SizedBox(
            width: 30.w, child: AppButton(title: "Cancel", action: Get.back)),
      ],
    );
  }
}
