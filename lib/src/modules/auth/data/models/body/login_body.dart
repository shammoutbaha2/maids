import 'package:maids/src/modules/auth/domain/entities/login_body.dart';

class LoginBodyModel extends LoginBody {
  LoginBodyModel({
    required super.username,
    required super.password,
  });

  factory LoginBodyModel.fromEntity(LoginBody body) => LoginBodyModel(
        username: body.username,
        password: body.password,
      );
  Map<String, dynamic> toJson() => {
        "username": username,
        "password": password,
        "expiresInMins": 10000,
      };
}
