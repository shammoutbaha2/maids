import 'package:maids/src/modules/auth/domain/entities/login_response.dart';

class LoginResponseModel extends LoginResponse {
  LoginResponseModel(
      {required super.username,
      required super.email,
      required super.id,
      required super.firstName,
      required super.gender,
      required super.image,
      required super.token,
      required super.lastName});

  factory LoginResponseModel.fromJson(Map<String, dynamic> json) =>
      LoginResponseModel(
          username: json["username"],
          id: json["id"],
          email: json["email"],
          firstName: json["firstName"],
          gender: json["gender"],
          image: json["image"],
          token: json["token"],
          lastName: json["lastName"]);
}
