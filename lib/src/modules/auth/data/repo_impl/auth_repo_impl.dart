import 'package:dartz/dartz.dart';
import 'package:maids/src/modules/auth/domain/entities/login_body.dart';

import '../../../../core/constant/const_values.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/errors/failures.dart';
import '../../../../core/functions/check_connection.dart';
import '../../../../core/helper/storage_helper.dart';
import '../../domain/repo/auth_repo.dart';
import '../data_source/auth_remote_data_source.dart';
import '../models/body/login_body.dart';
import '../models/resoponse/login_response.dart';

class AuthRepoImpl extends AuthRepo {
  final AuthRemoteDataSource remoteDataSource;
  final StorageHelper storageHelper;

  AuthRepoImpl({required this.remoteDataSource, required this.storageHelper});
  @override
  Future<Either<Failure, LoginResponseModel>> login(
      {required LoginBody loginBody}) async {
    if (await ConnectivityHelper.noConnection()) {
      return left(NetworkFailure(ConstantString.noInternetConnection));
    }
    try {
      final result = await remoteDataSource.login(
          loginBodyModel: LoginBodyModel.fromEntity(loginBody));

      await Future.wait([
        storageHelper.setString("token", result.token),
        storageHelper.setInt("userId", result.id)
      ]);

      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    } catch (e) {
      return Left(UnknownFailure(e.toString()));
    }
  }
}
