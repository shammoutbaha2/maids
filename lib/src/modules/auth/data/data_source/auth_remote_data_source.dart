import 'package:dio/dio.dart';
import 'package:maids/src/core/constant/const_values.dart';

import '../../../../core/errors/error_hdl.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/helper/dio_helper.dart';
import '../models/body/login_body.dart';
import '../models/resoponse/login_response.dart';

abstract class AuthRemoteDataSource {
  Future<LoginResponseModel> login({required LoginBodyModel loginBodyModel});
}

class AuthRemoteDataSourceImpl extends AuthRemoteDataSource {
  final DioHelper client;
  AuthRemoteDataSourceImpl({required this.client});

  @override
  Future<LoginResponseModel> login(
      {required LoginBodyModel loginBodyModel}) async {
    try {
      const path = 'auth/login';
      final response = await client.post(
        path: path,
        body: loginBodyModel.toJson(),
        auth: false,
      );
      if (response.statusCode == 200) {
        final responseModel = LoginResponseModel.fromJson(response.data);
        return responseModel;
      } else {
        throw ConstantString.anErrorOccurred;
      }
    } on DioException catch (e) {
      throw handleError(e);
    } catch (e) {
      throw ServerException(e.toString());
    }
  }
}
