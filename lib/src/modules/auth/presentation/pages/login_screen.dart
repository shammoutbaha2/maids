// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:maids/src/core/extension/doubles.dart';
import 'package:maids/src/core/extension/strings.dart';
import 'package:maids/src/core/validators/app_validator.dart';
import 'package:maids/src/core/widgets/custom_text.dart';

import '../../../../core/service/sl.dart';
import '../../../../core/theme/theme.dart';
import '../../../../core/widgets/elevated_button.dart';
import '../manager/auth_controller.dart';
import '../widgets/clip.dart';
import '../widgets/text_field.dart';

class LogInScreen extends StatelessWidget {
  const LogInScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final authController = sl<AuthController>();
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          ClipPath(
              clipper: MyClip(),
              child: Material(
                  elevation: 40,
                  color: AppTheme.getColorScheme.onSurfaceVariant,
                  shadowColor: AppTheme.getColorScheme.primary,
                  child: SizedBox(
                    width: 100.w,
                    height: 24.h,
                  ))),
          Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: authController.formKey,
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 15.h),
                  Center(
                    child: SizedBox(
                            width: 40.w,
                            height: 40.w,
                            child: Image.asset('logo.png'.imagePath))
                        .animate()
                        .fadeIn(begin: 0, duration: 1.seconds),
                  ),
                  SizedBox(height: 5.h),
                  CustomText(
                      text: "Login To Your Account",
                      size: 16,
                      align: TextAlign.left),
                  SizedBox(height: 6.h),
                  AuthTextField(
                    type: TextInputType.name,
                    icon: Icons.person,
                    validator: AppValidator.validatorName,
                    text: 'Full Name',
                    onSaved: (value) => authController.body.username = value,
                  ),
                  SizedBox(height: 2.h),
                  AuthTextField(
                    type: TextInputType.visiblePassword,
                    icon: Icons.password,
                    isSecure: true,
                    validator: AppValidator.validatorName,
                    text: 'Password',
                    onSaved: (value) => authController.body.password = value,
                  ),
                  const SizedBox(height: 20),
                  SizedBox(height: 12.h),
                  Center(
                    child: SizedBox(
                      child: AppButton(
                        title: 'Login',
                        action: authController.login,

                        // () async {
                        //   if (formKey.currentState!.validate()) {
                        //     formKey.currentState!.save();
                        //     LoaderUtils lu = LoaderUtils(context);
                        //     try {
                        //       lu.startLoading();
                        //       await sl<AuthController>().login(body);
                        //       Navigator.of(context).pushNamedAndRemoveUntil(
                        //           "/products", (route) => false);
                        //     } catch (e) {
                        //       lu.stopLoading();
                        //       toastErrorMessage(e.toString());
                        //     }
                        //   }
                        // }
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
