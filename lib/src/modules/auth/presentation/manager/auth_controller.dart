import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maids/src/core/routes/app_routes.dart';
import 'package:maids/src/core/widgets/loader_widget.dart';
import 'package:maids/src/core/widgets/scaffold_messenger.dart';
import 'package:maids/src/modules/auth/domain/entities/login_body.dart';

import '../../domain/usecases/login_uc.dart';

class AuthController extends Cubit {
  final LoginUC loginUc;
  AuthController({required this.loginUc}) : super(null);

  final LoginBody body = LoginBody();
  final formKey = GlobalKey<FormState>();

  Future login() async {
    if (!formKey.currentState!.validate()) return;
    formKey.currentState!.save();
    Get.showOverlay(
        asyncFunction: () async {
          final either = await loginUc(body);
          either.fold((l) => Toasts.toastMessage(l.message), (r) {
            Get.offAllNamed(Pages.tasks.route);
          });
        },
        loadingWidget: const LoadingWidget());
  }
}
