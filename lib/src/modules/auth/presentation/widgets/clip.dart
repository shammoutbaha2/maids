
import 'package:flutter/material.dart';

class MyClip extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    double w = size.width;
    double h = size.height;

    var path = Path();
    path.lineTo(0, h * 0.6);
    path.quadraticBezierTo(w * 0.15, h * 0.22, w * 0.8, h * 0.8);
    path.quadraticBezierTo(w * 0.95, h * 0.9, w, h * 0.75);
    path.lineTo(w, 0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => true;
}
