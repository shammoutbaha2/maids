import 'package:flutter/material.dart';

import '../../../../core/theme/theme.dart';

// ignore: must_be_immutable
class AuthTextField extends StatelessWidget {
  AuthTextField(
      {super.key,
      required this.onSaved,
      required this.icon,
      required this.validator,
      required this.text,
      this.type = TextInputType.text,
      this.maxLines = 1,
      this.initialValue,
      this.controller,
      this.isSecure});

  final Function(String?)? onSaved;
  final TextEditingController? controller;
  final String? Function(String?)? validator;
  final String text;
  final String? initialValue;
  final TextInputType type;
  final IconData icon;
  bool? isSecure;
  final int maxLines;
  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
      builder: (context, setState) => TextFormField(
        controller: controller,
        initialValue: initialValue,
        keyboardType: type,
        obscureText: isSecure == true,
        style: const TextStyle(color: Colors.black),
        cursorColor: AppTheme.getColor.primaryColor,
        maxLines: maxLines,
        decoration: InputDecoration(
          suffixIcon: isSecure == null
              ? Icon(icon, color: AppTheme.getColor.primaryColor)
              : isSecure!
                  ? InkWell(
                      onTap: () => setState(() {
                            isSecure = !isSecure!;
                          }),
                      child: Icon(Icons.remove_red_eye,
                          color: AppTheme.getColor.primaryColor))
                  : InkWell(
                      onTap: () => setState(() {
                            isSecure = !isSecure!;
                          }),
                      child: Icon(Icons.visibility_off_rounded,
                          color: AppTheme.getColor.primaryColor)),
          hintText: text,
          contentPadding:
              const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
          // icon: Icon(Icons.abc),
          hintStyle: TextStyle(
              color: AppTheme.getColor.primaryColor, fontSize: 14, height: 1),
          filled: true,
          fillColor: AppTheme.getColorScheme.onInverseSurface,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: const BorderSide(width: 0, color: Colors.transparent),
          ),
          errorStyle: TextStyle(color: AppTheme.getColorScheme.errorContainer),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(width: 0, color: Colors.transparent),
          ),
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(width: 0, color: Colors.transparent),
          ),
        ),
        validator: validator,
        onSaved: onSaved,
      ),
    );
  }
}
