import 'package:dartz/dartz.dart';
import 'package:maids/src/core/usecases/app_usecases.dart';
import 'package:maids/src/modules/auth/domain/entities/login_body.dart';
import 'package:maids/src/modules/auth/domain/entities/login_response.dart';

import '../../../../core/errors/failures.dart';
import '../repo/auth_repo.dart';

class LoginUC extends UseCaseTypeAndParams<LoginResponse, LoginBody> {
  final AuthRepo authRepositories;
  LoginUC({required this.authRepositories});
  @override
  Future<Either<Failure, LoginResponse>> call(LoginBody params) =>
      authRepositories.login(loginBody: params);
}
