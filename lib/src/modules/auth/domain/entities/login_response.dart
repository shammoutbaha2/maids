abstract class LoginResponse {
  final int id;
  final String username;
  final String email;
  final String firstName;
  final String gender;
  final String image;
  final String token;
  final String lastName;

  LoginResponse(
      {required this.username,
      required this.id,
      required this.email,
      required this.firstName,
      required this.gender,
      required this.image,
      required this.token,
      required this.lastName});
}
