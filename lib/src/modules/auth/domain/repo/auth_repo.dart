import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../entities/login_body.dart';
import '../entities/login_response.dart';

abstract class AuthRepo {
  Future<Either<Failure, LoginResponse>> login({required LoginBody loginBody});
}
