import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maids/src/core/extension/strings.dart';
import 'package:maids/src/modules/splash/presentations/manager/splash_controller.dart';

import '../../../../core/service/sl.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: sl<SplashController>(),
      builder: (context, state)=> Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: 120,
              child: Image.asset("tasks.png".imagePath)),
        ).animate().fadeIn(duration: 1.3.seconds, begin: 0),
      ),
    );
  }
}
