import 'package:get/get.dart';
import 'package:maids/src/core/base/base_cubit_controller.dart';
import 'package:maids/src/core/helper/storage_helper.dart';
import 'package:maids/src/core/routes/app_routes.dart';
import 'package:maids/src/core/service/sl.dart';

class SplashController extends BaseController {
  SplashController() : super(null);

  @override
  onInit() async {
    await 2.seconds.delay();
    if (sl<StorageHelper>().getString("token") == null) {
      return Get.offAllNamed(Pages.login.route);
    }
    Get.offAllNamed(Pages.tasks.route);
  }
}
