import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/services.dart';

class ConnectivityHelper {
  static Connectivity? _connectivity;
  static Future<bool> checkConnection() async {
    _init();

    ConnectivityResult result;
    try {
      result = (await connectivity.checkConnectivity()).first;
    } on PlatformException {
      return false;
    }
    if (result == ConnectivityResult.none) {
      return false;
    }
    return true;
  }

  static Future<bool> noConnection() async {
    _init();
    ConnectivityResult result;
    try {
      result = (await connectivity.checkConnectivity()).first;
    } on PlatformException {
      return true;
    }
    if (result == ConnectivityResult.none) {
      return true;
    }
    return false;
  }

  static _init() {
    _connectivity ??= Connectivity();
  }

  static Connectivity get connectivity => _connectivity!;
}
