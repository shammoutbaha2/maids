import 'package:get/get.dart';

class AppValidator {
  static String? validatorEmail(String str) {
    const String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    final RegExp regExp = RegExp(pattern);
    if (str.isEmpty) {
      return "Email Must Not Be Empty".tr;
    } else if (!regExp.hasMatch(str)) {
      return "Invalid Email Address".tr;
    }
    return null;
  }

  static String? validatorPass(String? str) {
    if (str == null || str.isEmpty) {
      return "Password Must Not Be Empty".tr;
    }
    if (!haveDigit(str) ||
        !haveLowercase(str) ||
        !haveUppercase(str) ||
        !haveNonAlphanumeric(str)) {
      return "Password Complexity Not Satisfied".tr;
    }
    return null;
  }

  static bool haveNonAlphanumeric(String str) {
    const String pattern = r'[^0-9a-zA-Z]';
    final RegExp regExp = RegExp(pattern);
    if (regExp.hasMatch(str)) {
      return true;
    }
    return false;
  }

  static bool haveLowercase(String str) {
    const String pattern = r'[a-z]';
    final RegExp regExp = RegExp(pattern);
    if (regExp.hasMatch(str)) {
      return true;
    }
    return false;
  }

  static String? validateLessThan8(String? str) {
    if (str == null || str.length < 8) return "Enter At Least 8 Character".tr;
    return null;
  }

  static bool haveDigit(String str) {
    const String pattern = r'[0-9]';
    final RegExp regExp = RegExp(pattern);
    if (regExp.hasMatch(str)) {
      return true;
    }
    return false;
  }

  static bool haveUppercase(String str) {
    const String pattern = r'[A-Z]';
    final RegExp regExp = RegExp(pattern);
    if (regExp.hasMatch(str)) {
      return true;
    }
    return false;
  }

  static int checkStrong(String str) {
    List<bool> all = [
      haveNonAlphanumeric(str),
      haveDigit(str),
      haveUppercase(str),
      haveLowercase(str),
    ];
    if (all.where((element) => element).length == 1) {
      return 1;
    }
    if (all.where((element) => element).length == 2) {
      return 2;
    }
    if (all.where((element) => element).length == 3) {
      return 3;
    }
    if (all.where((element) => element).length == 4) {
      return 4;
    }
    if (all.where((element) => element).length == 5) {
      return 5;
    }
    return 0;
  }

  static String? validatorName(String? str) {
    if (str == null || str.isEmpty) {
      return 'Can Not Be Empty'.tr;
    }
    return null;
  }

  static String? validatorPhone(String str) {
    const String pattern =
        r'^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$';
    const String pattern2 =
        r'^[\+]?[(]?[0-9]{4}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$';
    final RegExp regExp = RegExp(pattern);
    final RegExp regExp2 = RegExp(pattern2);
    if (str.isEmpty) {
      return 'PhoneIsRequired'.tr;
    } else if (!regExp.hasMatch(str) && !regExp2.hasMatch(str)) {
      return 'InvalidPhoneNumber'.trArgs(['']);
    }
    return null;
  }
}
