import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import '../../widgets/custom_text.dart';
import '../../widgets/error_widget.dart';
import '../../widgets/loader_widget.dart';
import '../manager/base_pagination.dart';

class PaginationListViewNew extends StatelessWidget {
  const PaginationListViewNew(
      {super.key,
      required this.controller,
      this.loaderLottie,
      this.noDataLottie,
      this.separatedWidget,
      required this.itemWidget});

  final PaginationCubit controller;
  final Widget Function(BuildContext, int) itemWidget;
  final String? noDataLottie;
  final String? loaderLottie;
  final Widget? separatedWidget;

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: controller,
      child:
          BlocBuilder<PaginationCubit, PaginationState>(builder: (ctx, state) {
        if (state is Loading) return const Center(child: LoadingWidget());
        if (state is ErrorState) {
          return ErrorComponent(text: state.error, action: controller.fetch);
        }
        return controller.data.isEmpty
            ? Center(child: CustomText(text: "Nothing to show".tr))
            : RefreshIndicator(
                triggerMode: RefreshIndicatorTriggerMode.anywhere,
                onRefresh: controller.refetchData,
                child: ListView.separated(
                    controller: controller.scrollController,
                    padding: const EdgeInsets.all(15),
                    separatorBuilder: (context, index) =>
                        separatedWidget ?? const SizedBox(),
                    itemCount: controller.data.length + 1,
                    itemBuilder: (context, index) {
                      if (index == controller.data.length) {
                        if (state is PaginationLoading) {
                          return const LoadingWidget(size: 10);
                        }
                        if (state is PaginationErrorState) {
                          return ErrorComponent(
                              text: state.error, action: controller.fetch);
                        }
                        return const SizedBox.shrink();
                      }

                      return itemWidget(context, index);
                    }));
      }),
    );
  }
}
