part of 'base_pagination.dart';

abstract class BasePaginationEvent {
  const BasePaginationEvent();
}

class LoadEvent extends BasePaginationEvent {}
class LoadMoreEvent extends BasePaginationEvent {}
class LoadedEvent extends BasePaginationEvent {}
class DoneEvent extends BasePaginationEvent {}
