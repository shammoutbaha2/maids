import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/widgets.dart';
import 'package:maids/src/core/errors/failures.dart';

import '../../widgets/scaffold_messenger.dart';

part 'base_pagination_event.dart';
part 'base_pagination_state.dart';

class PaginationBloc<T> extends Bloc<BasePaginationEvent, PaginationState> {
  PaginationBloc() : super(Initial()) {
    on<BasePaginationEvent>((event, emit) {});
    on<LoadEvent>((event, emit) async {
      emit(Loading());
      final either = await apiCall();
      if (either is Either) {
        either.fold((l) => emit(ErrorState(l.message, type: l.type!)),
            (r) => emit(Loaded<T>(data: r)));
      }
    });
  }

  apiCall() {}
}

abstract class PaginationCubit<T> extends Cubit<PaginationState> {
  PaginationCubit(super.initialState);
  int pageNumber = 1;
  int pageSize = 10;
  bool isOnePage = false;
  List<T> data = [];

  List get getData => data;

  Future<Either<Failure, List<T>>> apiCall();
  final ScrollController scrollController = ScrollController();

  Future<void> fetch() async {
    refreshData();
    await fetchData();
    scrollController.addListener(() async {
      if (scrollController.position.atEdge) {
        bool isTop = scrollController.position.pixels == 0;
        if (isTop) {
        } else {
          await fetchData();
        }
      }
    });
  }

  PaginationState paginationState = Initial();

  Future fetchData() async {
    if (isOnePage && data.isNotEmpty) return;
    try {
      if (state is Loading ||
          state is PaginationLoading ||
          paginationState is PaginationDone) return;
      emit(pageNumber == 1 ? Loading() : PaginationLoading());
      final response = await apiCall();
      response.fold((l) {
        throw l;
      }, (r) {
        if (r.length < pageSize) {
          paginationState = PaginationDone();
        } else {
          pageNumber++;
        }
        data.addAll(r);
      });
      emit(Loaded(data: data));
    } catch (e) {
      emit(pageNumber == 1
          ? ErrorState(e is Failure ? e.message : e.toString())
          : PaginationErrorState(e.toString()));

      Toasts.toastErrorMessage(e is Failure ? e.message : e.toString());
    }
  }

  Future refreshData() async {
    pageNumber = 1;
    data = [];
    paginationState = Initial();
    emit(Initial());
  }

  Future refetchData() async {
    refreshData();
    fetchData();
  }
}
