part of 'base_pagination.dart';

@immutable
abstract class PaginationState {
  const PaginationState();
}

class Initial extends PaginationState {}

class Loading extends PaginationState {}

class Loaded<T> extends PaginationState {
  final T? data;
  final Function? onIt;
  const Loaded({this.data, this.onIt});
}

class PaginationLoading extends PaginationState {}

class PaginationLoaded extends PaginationState {}

class PaginationDone extends PaginationState {}

class ErrorState extends PaginationState {
  final String error, type;
  final Function? onIt;

  const ErrorState(this.error, {this.type = '', this.onIt});
}

class PaginationErrorState extends PaginationState {
  final String error, type;
  final Function? onIt;

  const PaginationErrorState(this.error, {this.type = '', this.onIt});
}
