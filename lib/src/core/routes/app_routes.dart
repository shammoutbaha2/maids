import 'package:get/get.dart';
import 'package:maids/src/modules/auth/presentation/pages/login_screen.dart';
import 'package:maids/src/modules/splash/presentations/pages/splash_screen.dart';
import 'package:maids/src/modules/tasks/presentation/pages/my_tasks_screen.dart';

import '../../modules/tasks/presentation/pages/tasks_screen.dart';

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
      name: "/",
      page: () => const SplashScreen(),
    ),
    GetPage(
      name: Pages.login.route,
      page: () => const LogInScreen(),
    ),
    GetPage(
      name: Pages.tasks.route,
      page: () => const TasksScreen(),
    ),
    GetPage(
      name: Pages.myTasks.route,
      page: () => const MyTasksScreen(),
    ),
  ];
}

enum Pages {
  splash,
  login,
  home,
  tasks,
  myTasks;

  String get route => "/$name";
}
