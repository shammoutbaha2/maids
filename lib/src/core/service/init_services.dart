import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../helper/dio_helper.dart';
import '../helper/storage_helper.dart';

Future<void> initCore(GetIt sl) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();

  sl.registerLazySingleton<DioHelper>(
    () => DioHelperImpl(client: sl(), storage: sl()),
  );
  sl.registerLazySingleton(() => Dio());

  sl.registerLazySingleton<StorageHelper>(
      () => StorageHelperImpl(sharedPreferences: preferences));
}
