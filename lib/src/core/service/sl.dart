import 'package:get_it/get_it.dart';
import 'package:maids/src/modules/splash/presentations/manager/splash_controller.dart';

import 'auth_services.dart';
import 'init_services.dart';
import 'todo_services.dart';

final sl = GetIt.instance;

Future<void> initSL() async {
  await initCore(sl);
  sl.registerLazySingleton<SplashController>(
      () => SplashController()..onInit());
  initAuth(sl);
  initTodo(sl);
}
