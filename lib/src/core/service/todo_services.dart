import 'package:get_it/get_it.dart';
import 'package:maids/src/modules/tasks/data/data_source/todo_local_ds.dart';
import 'package:maids/src/modules/tasks/data/data_source/todo_remote_ds.dart';
import 'package:maids/src/modules/tasks/data/repo/todo_repo.dart';
import 'package:maids/src/modules/tasks/domain/repo/todo_repo.dart';
import 'package:maids/src/modules/tasks/domain/usecases/add_todo.dart';
import 'package:maids/src/modules/tasks/domain/usecases/delete_todo.dart';
import 'package:maids/src/modules/tasks/domain/usecases/edit_todo.dart';
import 'package:maids/src/modules/tasks/domain/usecases/get_my_tasks.dart';
import 'package:maids/src/modules/tasks/domain/usecases/get_todo.dart';
import 'package:maids/src/modules/tasks/presentation/manager/my_tasks_controller.dart';
import 'package:maids/src/modules/tasks/presentation/manager/todo_controller.dart';

Future<void> initTodo(GetIt sl) async {
  // Data Sources
  sl.registerLazySingleton<TodoRemoteDS>(
    () => TodoRemoteDSImpl(client: sl()),
  );
  sl.registerLazySingleton<TodoLocalDataSource>(
    () => TodoLocalDataSource(),
  );
  await sl<TodoLocalDataSource>().initDatabase();
  // Use Cases
  sl.registerLazySingleton<GetTodoUC>(() => GetTodoUC(repo: sl()));
  sl.registerLazySingleton<AddTodoUC>(() => AddTodoUC(repo: sl()));
  sl.registerLazySingleton<DeleteTodoUC>(() => DeleteTodoUC(repo: sl()));
  sl.registerLazySingleton<EditTodoUC>(() => EditTodoUC(repo: sl()));
  sl.registerLazySingleton<GetMyTasks>(() => GetMyTasks(repo: sl()));

  // Repository
  sl.registerLazySingleton<TodoRepo>(
      () => TodoRepoImpl(localDataSource: sl(), remoteDS: sl()));

  // controller
  sl.registerLazySingleton<TodoController>(
      () => TodoController(getTodoUC: sl())..fetch());
  sl.registerLazySingleton<MyTasksController>(() => MyTasksController(
      addTodoUC: sl(), editTodoUC: sl(), deleteTodoUC: sl(), getMyTasks: sl())
    ..fetch());
}
