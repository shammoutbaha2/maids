import 'package:get_it/get_it.dart';
import 'package:maids/src/modules/auth/data/data_source/auth_remote_data_source.dart';
import 'package:maids/src/modules/auth/data/repo_impl/auth_repo_impl.dart';
import 'package:maids/src/modules/auth/domain/repo/auth_repo.dart';
import 'package:maids/src/modules/auth/domain/usecases/login_uc.dart';
import 'package:maids/src/modules/auth/presentation/manager/auth_controller.dart';

initAuth(GetIt sl) {
  // Data Sources
  sl.registerLazySingleton<AuthRemoteDataSource>(
      () => AuthRemoteDataSourceImpl(client: sl()));

  // Use Cases
  sl.registerLazySingleton<LoginUC>(() => LoginUC(authRepositories: sl()));

  // Repository
  sl.registerLazySingleton<AuthRepo>(
      () => AuthRepoImpl(storageHelper: sl(), remoteDataSource: sl()));

  // controller
  sl.registerLazySingleton<AuthController>(() => AuthController(loginUc: sl()));
}
