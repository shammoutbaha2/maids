import 'package:dio/dio.dart';

import '../functions/print.dart';
import 'failures.dart';

String handleError(DioException e) {
  if (e.type == DioExceptionType.connectionTimeout) {
    return "Connection Time Out Try Again Later";
  } else if (e.type == DioExceptionType.receiveTimeout) {
    return "Receive Time Out Try Again Later";
  } else if (e.type == DioExceptionType.badResponse) {
    pr(e.response?.data);
    return (e.response?.data is Map)
        ? e.response?.data["msg"] ?? e.response?.data['message']
        : e.message;
  } else if (e.type == DioExceptionType.sendTimeout) {
    return "Send Time Out Try Again Later";
  } else {
    return "Unknown Error";
  }
}

String handleErrByType(Failure failure) {
  switch (failure.runtimeType) {
    case ServerFailure _:
    case NetworkFailure _:
      return failure.message;
    case CacheFailure _:
      return "Cache Error";
    default:
      return "Unknown Error";
  }
}
