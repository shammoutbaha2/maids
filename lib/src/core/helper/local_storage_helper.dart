abstract class DBHelper<T> {
  Future initDatabase();

  Future database();

  Future insert(T data);
  Future insertList(List<T> data);

  Future<List> getData({Map<String , dynamic>? query});

  Future<int> delete(int id);

  Future<int> update(T data);
}
