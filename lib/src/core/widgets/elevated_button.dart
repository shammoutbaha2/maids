import 'package:flutter/material.dart';

import '../theme/theme.dart';
import 'custom_text.dart';

class AppButton extends StatelessWidget {
  const AppButton(
      {super.key, required this.title, required this.action, this.color});
  final String title;
  final VoidCallback action;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    ButtonStyle elevatedButton = ElevatedButton.styleFrom(
      backgroundColor: color ?? AppTheme.getColorScheme.primary,
      elevation: 5,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    );
    return ElevatedButton(
      style: elevatedButton,
      onPressed: action,
      child: SizedBox(
        height: 50,
        child: Center(
          child: CustomText(
              text: title,
              color: Colors.white,
              size: 18,
              weight: FontWeight.w500),
        ),
      ),
    );
  }
}
