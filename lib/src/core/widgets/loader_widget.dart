import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:maids/src/core/extension/doubles.dart';
import 'package:maids/src/core/extension/strings.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({super.key, this.size});
  final double? size;

  @override
  Widget build(BuildContext context) {
    double size0 = (size ?? 30).w;

    return SizedBox.square(
      dimension: size0,
      child: Center(
        child:
            LottieBuilder.asset("loader".lottie, width: size0, height: size0),
      ),
    );
  }
}
