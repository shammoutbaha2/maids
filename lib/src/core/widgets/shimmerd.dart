import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class Shimmered extends StatelessWidget {
  const Shimmered(
      {super.key, required this.child, this.baseColor, this.highlightColor});
  final Widget child;
  final Color? baseColor;
  final Color? highlightColor;

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        baseColor: baseColor ?? Colors.grey[100]!,
        highlightColor: highlightColor ?? Colors.grey[300]!,
        child: child);
  }
}
