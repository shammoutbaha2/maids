import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:maids/src/core/extension/doubles.dart';
import 'package:maids/src/core/theme/theme.dart';

import 'custom_text.dart';

class ErrorComponent extends StatelessWidget {
  const ErrorComponent(
      {super.key, required this.text, this.width, this.height, this.action});
  final String text;

  final double? width;
  final double? height;
  final VoidCallback? action;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: CustomText(
              text: text,
              maxLines: 3,
              overflow: TextOverflow.visible,
            ),
          ),
          10.sizedH,
          Visibility(
              visible: action != null,
              child: InkWell(
                onTap: action,
                child: CustomText(
                  text: "Retry".tr,
                  decoration: TextDecoration.underline,
                  color: AppTheme.getColor.primaryColor,
                ),
              ))
        ],
      ),
    );
  }
}
