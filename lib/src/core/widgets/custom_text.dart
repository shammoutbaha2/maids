import 'package:flutter/material.dart';

import '../theme/theme.dart';

// ignore: must_be_immutable
class CustomText extends StatelessWidget {
  CustomText(
      {super.key,
      required this.text,
      this.color,
      this.width,
      this.maxLines,
      this.size = 15,
      this.overflow = TextOverflow.visible,
      this.fontFamily,
      this.decoration,
      this.align = TextAlign.center,
      this.weight = FontWeight.w500});

  Color? color;
  final String text;
  final double? width;
  int? maxLines;
  double size;
  TextAlign align;
  FontWeight weight;
  TextOverflow overflow;
  String? fontFamily;
  TextDecoration? decoration;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: width == null ? null : 0.01 * width!,
        child: Text(
          text,
          maxLines: maxLines,
          textAlign: align,
          style: TextStyle(
            overflow: overflow,
            color: color ?? AppTheme.getColorScheme.onBackground,
            fontFamily: fontFamily,
            decoration: decoration,
            fontSize: size,
            fontWeight: weight,
          ),
        ));
  }
}
