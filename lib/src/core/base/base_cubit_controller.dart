import 'package:bloc/bloc.dart';

abstract class BaseController<T> extends Cubit<T> {
  BaseController(super.initialState);

  onInit();
  onDispose() {}
}
