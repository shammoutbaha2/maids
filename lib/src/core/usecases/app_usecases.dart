import 'package:dartz/dartz.dart';
import 'package:maids/src/core/errors/failures.dart';

abstract class UseCaseTypeAndParams<T, P> {
  Future<Either<Failure, T>> call(P params);
}
abstract class UseCaseType<T> {
  Future<Either<Failure, T>> call();
}
