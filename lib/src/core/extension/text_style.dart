import 'package:flutter/material.dart';

extension FontWeightEx on int {
  get w1 => FontWeight.w100;
  get w2 => FontWeight.w200;
  get w3 => FontWeight.w300;
  get w4 => FontWeight.w400;
  get w5 => FontWeight.w500;
  get w6 => FontWeight.w600;
  get w7 => FontWeight.w700;
  get w8 => FontWeight.w800;
  get w9 => FontWeight.w900;
  get wB => FontWeight.bold;
  get wN => FontWeight.normal;
}
