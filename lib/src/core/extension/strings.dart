import 'package:flutter/material.dart';
// import 'package:get/get.dart';

extension StringEx on String {
  Widget get text => Text(this);
  String get imagePath => "assets/images/$this";
  String get lottie => "assets/animation/$this.json";
  String get svgPath => "assets/icons/svg/$this.svg";
  String get iconPath => "assets/icons/$this";
}
