// extension MapEx on Map<String, dynamic> {
//   _() {
//     var map = {
//       'email': 'example@example.com',
//       'name': 'John Doe',
//     };

//     var wrappedMap = MapWrapper(map);

//     var email = wrappedMap.email;
//     var name = wrappedMap.name;
//   }
// }

// class MapWrapper {
//   final Map<String, dynamic> _map;

//   MapWrapper(this._map);

//   @override
//   dynamic noSuchMethod(Invocation invocation) {
//     String key = invocation.memberName
//         .toString()
//         .replaceFirst('Symbol("', '')
//         .replaceFirst('")', '');
//     return _map[key];
//   }
// }
