import 'package:flutter/material.dart';
import 'package:get/get.dart';

extension DoubleEx on num {
  Widget get sizedH => SizedBox(height: h);
  Widget get sizedW => SizedBox(width: w);
  Widget get wSize => SizedBox(width: h);
  Widget get hSize => SizedBox(height: w);
  double get w => Get.width * this / 100;
  double get h => Get.height * this / 100;
}
