import 'package:flutter/material.dart';

import '../widgets/shimmerd.dart';

extension WidgetProperties on Widget {
  Widget scale(double scale) {
    return AnimatedScale(scale: scale, duration: Duration.zero, child: this);
  }

  Padding padding(
          {double? all, double? hor, double? ver, double? left}) =>
      Padding(
          padding: all != null
              ? EdgeInsets.all(all)
              : EdgeInsets.symmetric(horizontal: hor ?? 0, vertical: ver ?? 0),
          child: this);

  InkWell onTap(void Function()? onIt) => InkWell(onTap: onIt, child: this);

  Shimmered get shimmered => Shimmered(child: this);
  Shimmered coloredShimmered(Color baseColor, Color highLight) =>
      Shimmered(baseColor: baseColor, highlightColor: highLight, child: this);
}
