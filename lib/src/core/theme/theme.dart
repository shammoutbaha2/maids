import 'package:flutter/material.dart';

@immutable
class AppTheme {
  static ThemeData base() {
    return ThemeData(
      fontFamily: "Changa",
      useMaterial3: true,
      scaffoldBackgroundColor: Colors.white,
      primaryColor: const Color(0xFF3964E6), // Primary color
      colorScheme: const ColorScheme.dark(
        brightness: Brightness.dark,
        primary: Color(0xFF3964E6), // Primary color
        onPrimary: Color(0xFFFFFFFF), // Text color on primary
        primaryContainer: Color.fromARGB(255, 229, 191, 103), // Accent color
        onPrimaryContainer: Color(0xFF1D3557), // Text color on accent
        secondary: Color(0xFF171717), // Secondary color
        onSecondary: Color(0xFFFFFFFF), // Text color on secondary
        secondaryContainer: Color(0xFFA8DADC), // Secondary container color
        onSecondaryContainer:
            Color(0xFF457B9D), // Text color on secondary container
        tertiary: Color(0xFF2A9D8F), // Tertiary color
        onTertiary: Color(0xFFFFFFFF), // Text color on tertiary
        tertiaryContainer: Color(0xFF264653), // Tertiary container color
        onTertiaryContainer:
            Color(0xFFFFFFFF), // Text color on tertiary container
        error: Color(0xFFBA1A1A), // Error color
        onError: Color(0xFFFFFFFF), // Text color on error
        errorContainer: Color(0xFFE76F51), // Error container color
        onErrorContainer: Color(0xFF410002), // Text color on error container
        background: Color(0xFFF1FAEE), // Background color
        onBackground: Color(0xFF1D3557), // Text color on background
        surface: Color.fromARGB(255, 229, 191, 103), // Surface color
        onSurface: Color(0xFF1D3557), // Text color on surface
        surfaceVariant: Color(0xFFE5E5E5), // Surface variant color
        onSurfaceVariant: Color(0xFF6C757D), // Text color on surface variant
        outline: Color(0xFFADB5BD), // Outline color
        shadow: Color(0xFF000000), // Shadow color
        inverseSurface: Color(0xFF343A40), // Inverse surface color
        onInverseSurface: Color(0xFFF8F9FA), // Text color on inverse surface
        inversePrimary: Color(0xFFE63946), // Inverse primary color
        surfaceTint: Color(0xFFE63946), // Surface tint color
      ),
    );
  }

  static ThemeData get getColor {
    return base();
  }

  static ColorScheme get getColorScheme {
    return base().colorScheme;
  }
}
