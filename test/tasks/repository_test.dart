import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:maids/src/core/errors/failures.dart';
import 'package:maids/src/modules/tasks/data/repo/todo_repo.dart';
import 'package:maids/src/modules/tasks/domain/entities/manage_todo.dart';
import 'package:maids/src/modules/tasks/domain/entities/task_entity.dart';
import 'package:maids/src/modules/tasks/domain/repo/todo_repo.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'repository_test.mocks.dart';

@GenerateMocks([TodoRepo, TodoRepoImpl])
void main() async {
  group("Testing Repository for todo", () {
    final todo = ManageTodo(
        todo: "Test Todo",
        completed: false,
        todoId: DateTime.now().millisecond,
        userId: 15);
    late MockTodoRepoImpl mockTodoRepo;

    setUp(() {
      mockTodoRepo = MockTodoRepoImpl();
    });
    test("Add New To DO ", () async {
      when(mockTodoRepo.addTodo(todo: todo)).thenAnswer((value) async {
        return const Right(unit);
      });
      final result = await mockTodoRepo.addTodo(todo: todo);
      expect(result, const Right(unit));
    });
    test("Edit To DO ", () async {
      when(mockTodoRepo.editTodo(todo: todo)).thenAnswer((value) async {
        return const Right(unit);
      });
      final result = await mockTodoRepo.editTodo(todo: todo);
      expect(result, const Right(unit));
    });
    test("Delete To DO ", () async {
      when(mockTodoRepo.deleteTodo(id: todo.todoId)).thenAnswer((value) async {
        return const Right(unit);
      });
      final result = await mockTodoRepo.deleteTodo(id: todo.todoId);
      expect(result, const Right(unit));
    });
    test("Fetch To DO ", () async {
      when(mockTodoRepo.getTodo(page: 1)).thenAnswer((value) async {
        return const Right([]);
      });
      final result = await mockTodoRepo.getTodo(page: 1);
      if (result.isRight()) {
        expect(result, const Right<Failure, List<Todo>>([]));
      } else {
        expect(result, Failure);
      }
    });
  });
}
