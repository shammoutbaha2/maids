import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:maids/src/core/helper/dio_helper.dart';
import 'package:maids/src/core/helper/storage_helper.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'dio_test.mocks.dart';

@GenerateMocks([DioHelperImpl, StorageHelperImpl, Dio])
void main() {
  group('DioHelperImpl', () {
    late DioHelperImpl dioHelper;
    late MockDio mockDio;
    late MockStorageHelperImpl mockStorageHelper;

    setUp(() {
      mockDio = MockDio();
      mockStorageHelper = MockStorageHelperImpl();
      dioHelper = DioHelperImpl(client: mockDio, storage: mockStorageHelper);
    });

    const baseURL = 'https://dummyjson.com'; // Define baseURL
    const path = "/test";
    test('get sends a GET request with correct parameters', () async {
      // Arrange
      when(mockDio.get(
        baseURL + path, // Concatenate baseURL and path
        queryParameters: null,
        options: anyNamed('options'),
      )).thenAnswer((_) async =>
          Response(data: 'Response Data', requestOptions: RequestOptions()));

      // Act
      final response = await dioHelper.get(path: path);

      // Assert
      verify(mockDio.get(
        baseURL + path, // Concatenate baseURL and path
        options: anyNamed('options'),
      ));
      expect(response.data, 'Response Data');
    });

    // test('post sends a POST request with correct parameters', () async {
    //   // Arrange

    //   final body = {'key': 'value'};
    //   when(mockDio.post(any, data: anyNamed('data'), options: Options()))
    //       .thenAnswer((_) async => Response(
    //           data: 'Response Data', requestOptions: RequestOptions()));

    //   // Act
    //   final response = await dioHelper.post(path: path, body: body);

    //   // Assert
    //   verify(mockDio.post(
    //     any,
    //     data: body,
    //     queryParameters: null,
    //     options: Options(headers: {}),
    //   ));
    //   expect(response.data, 'Response Data');
    // });

    // // Similarly, write tests for update and delete methods...

    // test('update sends a PUT request with correct parameters', () async {
    //   // Arrange

    //   final body = {'key': 'value'};
    //   when(mockDio.put(any, data: anyNamed('data'), options: Options()))
    //       .thenAnswer((_) async => Response(
    //           data: 'Response Data', requestOptions: RequestOptions()));

    //   // Act
    //   final response = await dioHelper.update(path: path, body: body);

    //   // Assert
    //   verify(mockDio.put(
    //     any,
    //     data: body,
    //     queryParameters: null,
    //     options: Options(headers: {}),
    //   ));
    //   expect(response.data, 'Response Data');
    // });

    // test('delete sends a DELETE request with correct parameters', () async {
    //   // Arrange

    //   final body = {'key': 'value'};
    //   when(mockDio.delete(any, data: anyNamed('data'), options: Options()))
    //       .thenAnswer((_) async => Response(
    //           data: 'Response Data', requestOptions: RequestOptions()));

    //   // Act
    //   final response = await dioHelper.delete(path: path, body: body);

    //   // Assert
    //   verify(mockDio.delete(
    //     any,
    //     data: body,
    //     queryParameters: null,
    //     options: Options(headers: {}),
    //   ));
    //   expect(response.data, 'Response Data');
    // });
  });
}
