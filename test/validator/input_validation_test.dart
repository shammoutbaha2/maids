import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:maids/src/core/validators/app_validator.dart';

void main() {
  group('AppValidator', () {
    test('validatorEmail returns error message for empty email', () {
      final result = AppValidator.validatorEmail('');
      expect(result, 'Email Must Not Be Empty'.tr);
    });

    test('validatorEmail returns error message for invalid email format', () {
      final result = AppValidator.validatorEmail('invalid_email');
      expect(result, 'Invalid Email Address'.tr);
    });

    test('validatorEmail returns null for valid email format', () {
      final result = AppValidator.validatorEmail('valid@email.com');
      expect(result, isNull);
    });

    test('validatorPass returns error message for empty password', () {
      final result = AppValidator.validatorPass('');
      expect(result, 'Password Must Not Be Empty'.tr);
    });

    test(
        'haveNonAlphanumeric returns true for string with non-alphanumeric characters',
        () {
      final result = AppValidator.haveNonAlphanumeric('abc@123');
      expect(result, isTrue);
    });
  });
}
